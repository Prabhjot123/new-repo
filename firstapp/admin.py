from django.contrib import admin
from .models import user,user1,student,Account,register

class studentAdmin(admin.ModelAdmin):
     fields=['roll_no','name']
     search_fields=['name']
     list_filter=['name']
     list_display=['roll_no','name']
     list_editable=['name']
admin.site.register(user)
admin.site.register(user1)
admin.site.register(student,studentAdmin)
admin.site.register(Account)
admin.site.register(register)

# Register your models here.
