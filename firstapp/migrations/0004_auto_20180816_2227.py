# Generated by Django 2.0.5 on 2018-08-17 05:27

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('firstapp', '0003_auto_20180816_2222'),
    ]

    operations = [
        migrations.AlterField(
            model_name='student',
            name='website',
            field=models.URLField(blank=True),
        ),
    ]
