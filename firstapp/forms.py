from django import form

class studentmsform(forms.form):
    name=forms.CharField()
    password=forms.CharField()
    confirm=forms.CharField()
    email=forms.EmailField()
    Roll_No=forms.IntegerField()
    about=forms.CharField()
    register=forms.BooleanField()


    def clean_name(myform):
        name = myform.cleaned_data['name']
        if len(name)>5:
            raise forms.ValidationError("name length should be less than 5")
        else:
            return name
    def clean(self):
        allc = super().clean()
        p = allc['password']
        c = allc['confirm']
        if c!=p:
            raise forms.ValidationError("does not match")
        else:
            return c + p    
