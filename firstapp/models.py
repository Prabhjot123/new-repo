from django.db import models

# Create your models here.
class user(models.Model):
    name=models.CharField(max_length=50)
    contact=models.IntegerField(unique= True)

    def __str__(self):
       return self.name+'contact='+ repr(self.contact)

class user1(models.Model):
    name=models.CharField(max_length=50)
    contact=models.IntegerField(unique= True)

class student(models.Model):
    BG=(
    ('A+','A+'),
    ('B+','B+'),
    ('O+','O+'),
    ('AB+','AB+'),
    ('O-','O-'),
    )
    name=models.CharField(max_length=50)
    roll_no=models.IntegerField(unique=True)
    email=models.EmailField(max_length=150 , unique=True)
    contact=models.IntegerField()
    Date_of_birth=models.DateField()
    website=models.URLField(blank=True)
    blood_group=models.CharField(max_length=4 ,choices=BG)
    about=models.TextField(null=True)
    gender=models.BooleanField("Male", blank=True, default = True)
    female=models.BooleanField("Female", blank=True, default = False)

    def __str__(self):
        return self.name+'contact='+ repr(self.contact)
    class Meta:
       verbose_name_plural='student'
class Account(models.Model):
    roll_no=models.ForeignKey(student , on_delete=models.CASCADE)
    fee_paid=models.IntegerField()
    fee_pending=models.IntegerField()
    submission_date=models.DateField(auto_now_add=True)

    def __str__(self):
      return repr(self.roll_no)

class register(models.Model):
    full_name = models.CharField(max_length=50)
    password = models.CharField(max_length=17)
    email = models.EmailField(max_length=150)

    def __str__(self):
        return self.full_name
